import React, { FC } from 'react'
import { ReactComponent as CloseBtn } from './icons/closeBtn.svg'

import './Modal.scss'

const Modal: FC = (props) => {
    return (
        <div className="modal-wrapper">
            <div className="modal">
                <div className="modal-box">
                    <button type="button" className="modal-close">
                        <CloseBtn />
                    </button>
                    <div className="modal-header">
                        <h4>Modal</h4>
                    </div>
                    <div className="modal-content">
                КІНО => {title}
                    </div>
                    <div className="modal-footer">
                        <div className="button-wrapper">
                            <button className="btn" type="button" >OK</button>
                            <button className="btn" type="button" >Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal