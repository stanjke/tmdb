import React, { FC } from 'react'
import './Footer.scss'

const Footer: FC = () => {
    return (
        <footer className="g-footer">
            <div className="copyright">copyright (c) 2023</div>
        </footer>
    )
}

export default Footer