import React, { FC } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import MovieTv from './components/MovieTv/MovieTv';
import MovieCinema from './components/MovieCinema/MovieCinema';
import './Movies.scss'

const Movies: FC = () => {
    return (
        <>
            <div className="films__title">Popular on TV</div>
            <div className="films__slider">
                <MovieTv />
            </div>
            <div className="films__title">Popular in Cinema</div>
            <div className="films__slider">
                <MovieCinema />
            </div>
        </>
    )
}

export default Movies