import React, { FC } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import { useGetCinemaMoviesQuery } from '../../../../store/serverRespond/fetchMoviesApi';
import { IMG_URL } from '../../../../configs/API';
import { ICinema } from '../../../../types/types';
import Button from '../../../Button/Button';
import { useActions } from '../../../../hooks/useActions';


import 'swiper/css';
import { useDispatch } from 'react-redux';
import { addToFavorites } from '../../../../store/favorites/favorites.slice';


const MovieCinema: FC = () => {

    const { data, isLoading } = useGetCinemaMoviesQuery(undefined);

    const dispatch = useDispatch()



    const cinemaCards = data?.map((film: ICinema) => {
        return (
            <SwiperSlide key={film.id} className="film__item">
                <div className="film-poster">
                    <img src={`${IMG_URL}${film.poster_path}`} alt={film.original_title} />
                    <div className="film-poster-back">
                        <h1 className="film-poster__title">{film.title}</h1>
                        <p className="film-poster__subtitle"><i>{film.original_title}</i></p>
                        <p className="film-poster__desc">{film.overview}</p>
                        <div className="button__wrapper">
                            <Button to={`/cinema/${film.id}`}>More </Button>
                            <Button info={film} action={addToFavorites}>Favorites </Button>
                        </div>
                    </div>
                </div>
            </SwiperSlide>
        )
    })




    return (
        <Swiper
            slidesPerView={5}
            spaceBetween={16}
            className="films__wrapper"
            navigation={true}
            grabCursor={false}
            draggable={false}
            preventClicksPropagation={true}
            preventClicks={true}
            scrollbar={{ draggable: false, hide: true }}
            slideToClickedSlide={false}
            pagination={{ clickable: true }}
        >
            {isLoading ? <div>Loading...</div> : cinemaCards}
        </Swiper>
    )
}

export default MovieCinema