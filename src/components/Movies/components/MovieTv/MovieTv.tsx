import React, { FC } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import { useGetTvMoviesQuery } from '../../../../store/serverRespond/fetchMoviesApi';
import { ITv } from '../../../../types/types';
import { IMG_URL } from '../../../../configs/API';

import 'swiper/css';
import Button from '../../../Button/Button';

const MovieTv: FC = () => {


    const { data } = useGetTvMoviesQuery(undefined)

    const tvCards = data?.map((film: ITv) => {
        return (

            <SwiperSlide key={film.id} className="film__item">
                <div className="film-poster">
                    <img src={`${IMG_URL}${film.poster_path}`} alt={film.original_name} />
                    <div className="film-poster-back">
                        <h1 className="film-poster__title">{film.name}</h1>
                        <p className="film-poster__subtitle"><i>{film.original_name}</i></p>
                        <p className="film-poster__desc">{film.overview}</p>
                        <div className="button__wrapper">
                            <Button to={`/tv/${film.id}`} />
                        </div>
                    </div>
                </div>

            </SwiperSlide>

        )
    })

    return (
        <Swiper
            slidesPerView={5}
            spaceBetween={16}
            className="films__wrapper"
            navigation={true}
            grabCursor={false}
            draggable={false}
            preventClicksPropagation={true}
            preventClicks={true}
            scrollbar={{ draggable: false, hide: true }}
            slideToClickedSlide={false}
            pagination={{ clickable: true }}
        >
            {tvCards}
        </Swiper>
    )
}

export default MovieTv