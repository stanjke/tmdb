import { FC } from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as TmdbLogo } from './icons/tmdbLogo.svg'
import { ReactComponent as Heart } from './icons/Heart.svg'

import './Header.scss'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { useActions } from '../../hooks/useActions'

const Header: FC = () => {

    const favorites = useTypedSelector(state => state.favorites)



    return (
        <header className="header">
            <div className="container">
                <div className="header__wrapper">
                    <div className="header__logo">
                        <Link to="/" className='logo'>
                            <TmdbLogo />
                        </Link>
                    </div>
                    <ul className="header__nav">
                        <li>
                            <Link to="/" className='nav__link'>Home</Link>
                        </li>
                        <li>
                            <Link to="/favorites" className='nav__link'>Favorites</Link>
                        </li>
                        <li>
                            <Link to="/user" className='nav__link'>User</Link>
                        </li>
                    </ul>
                    <div className="header__actions">
                        <div className="header__favorites-list">
                            Favorites list
                            <span className="icon-favorite">
                                <span className="count">{favorites.length}</span>
                                <Heart />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header