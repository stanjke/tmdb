import React, { FC, useState } from 'react'
import { IButton } from '../../types/types'
import { Link } from 'react-router-dom'
import cn from 'classnames'

import './Button.scss'
import { useDispatch } from 'react-redux'

const Button: FC<IButton> = ({ classNames, type, action, to, href, children, restProps, info }) => {

    const dispatch = useDispatch()
    const [isEvent, setIsEvent] = useState(false)
    const handleEvent = () => {
        setIsEvent(!isEvent)
    }
    const handleClick = () => {
        if (info) {
            console.log(action);
            console.log(info);
            dispatch(action(info))
        }
    }

    let Component = href ? 'a' : 'button'

    if (to) {
        Component = Link;
    }

    return (
        <Component
            className={cn('btn', classNames, { active: isEvent })}
            type={type}
            onClick={handleClick}
            href={href}
            to={to}
            {...restProps}>
            {children}
        </Component>
    );
}

Button.defaultProps = {
    type: 'button',
}

export default Button