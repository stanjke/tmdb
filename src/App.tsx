import { FC } from 'react'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import Movies from './components/Movies/Movies'
import { Routes, Route } from 'react-router-dom'


import './App.css'
import Layout from './components/Layout/Layout'
import UserPage from './pages/UserPage/UserPage'
import FavoritesPage from './pages/FavoritesPage/FavoritesPage'
import TvMoviePage from './pages/TvMoviePage/TvMoviePage'
import NotPage from './pages/NotPage/NotPage'
import CinemaMoviePage from './pages/CinemaMoviePage/CinemaMoviePage'

const App: FC = () => {

  return (
    <>

      <Routes>
        <Route path='/' element={<Layout />}>
          <Route index element={<Movies />} />
          <Route path='tv/:tvId' element={<TvMoviePage />} />
          <Route path='cinema/:cinemaId' element={<CinemaMoviePage />} />
          <Route path='user' element={<UserPage />} />
          <Route path='favorites' element={<FavoritesPage />} />
          <Route path='*' element={<NotPage />} />
        </Route>
      </Routes>

    </>
  )
}

export default App
