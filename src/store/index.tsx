import { configureStore } from '@reduxjs/toolkit'
import { fetchMoviesApi } from './serverRespond/fetchMoviesApi'
import favoritesSlice from './favorites/favorites.slice'

export const store = configureStore({
    reducer: {
        favorites: favoritesSlice,
        [fetchMoviesApi.reducerPath]: fetchMoviesApi.reducer,

    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(fetchMoviesApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>