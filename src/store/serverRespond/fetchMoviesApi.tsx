import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { API_KEY_3, API_URL } from '../../configs/API'
import { ICinemaInfo, ICinemaMovieResponse, ITvInfo, ITvInfoSubset, ITvMovieResponse } from '../../types/types'

export const fetchMoviesApi = createApi({
    reducerPath: 'moviesApi',
    baseQuery: fetchBaseQuery({ baseUrl: `${API_URL}` }),
    endpoints: (builder) => ({
        getTvMovies: builder.query({
            query: () => `/discover/tv?api_key=${API_KEY_3}`,
            transformResponse: (response: ITvMovieResponse) => response.results,
        }),
        getTvMovieInfo: builder.query<ITvInfo, number>({
            query: (id) => `${API_URL}/tv/${id}?api_key=${API_KEY_3}`,
        }),
        getCinemaMovies: builder.query({
            query: () => `/discover/movie?api_key=${API_KEY_3}`,
            transformResponse: (response: ICinemaMovieResponse) => response.results,
        }),
        getCinemaMovieInfo: builder.query<ICinemaInfo, number>({
            query: (id) => `${API_URL}/movie/${id}?api_key=${API_KEY_3}`,
        }),

    })
})

export const {
    useGetTvMoviesQuery,
    useGetCinemaMoviesQuery,
    useGetTvMovieInfoQuery,
    useGetCinemaMovieInfoQuery,
} = fetchMoviesApi
