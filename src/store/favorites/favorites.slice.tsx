import { PayloadAction, createSlice } from "@reduxjs/toolkit";


const initialState: any = [];



const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    reducers: {
        addToFavorites: (state, { payload }) => {
            if (state.length === 0) {
                state.push(payload)
            } else if (!state.some(item => item.id !== payload.id)) {
                state.push(payload)
            }
        },
        removeFromFavorites: (state, { payload }) => {
            state.filter(item => item.id !== payload.id)
        }
    },
})

export const { addToFavorites, removeFromFavorites } = favoritesSlice.actions

export default favoritesSlice.reducer