import React, { FC } from 'react'
import { useParams } from 'react-router-dom'
import { useGetCinemaMovieInfoQuery } from '../../store/serverRespond/fetchMoviesApi'
import { IMG_URL } from '../../configs/API'

const CinemaMoviePage: FC = () => {

    const { cinemaId: id } = useParams()

    const { data, isError, isLoading } = useGetCinemaMovieInfoQuery(Number(id))

    if (isLoading) { return <div>Loading...</div> }
    if (isError) { return <div>Error...</div> }
    if (!data) { return null }

    const {
        backdrop_path,
        poster_path,
        original_title,
        title,
        genres,
        runtime,
        overview,
        release_date
    } = data

    const genresItems = genres?.map(item => <span key={item.id}>{item.name}</span>)

    return (
        <header className="page-movie-header">
            <div className="header-bg" style={{ backgroundImage: `url(${IMG_URL}${backdrop_path})` }} />
            <div className="container">
                <div className="header-wrapper">
                    <span className="btn-back"></span>
                    <div className="header-poster">
                        <img src={`${IMG_URL}${poster_path}`} alt={title} />
                    </div>
                    <div className="header-content">
                        <p className="movie-name">{title}</p>
                        <p className="movie-subname"><i>{original_title}</i></p>
                        <p className="movie-info">
                            <span className="genres">
                                {genresItems}
                            </span>
                            <span className="runtime">{runtime}</span>
                        </p>
                        <p className="movie-overview">
                            {overview}
                        </p>
                        <p className="movie-release">
                            Release date: {release_date}
                        </p>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default CinemaMoviePage