import React from 'react'
import { useParams } from 'react-router-dom'
import { useGetTvMovieInfoQuery } from '../../store/serverRespond/fetchMoviesApi'
import { ITvInfo } from '../../types/types'
import { IMG_URL } from '../../configs/API'
import './TvMoviePage.scss'

const TvMoviePage = () => {

    const { tvId: id } = useParams()

    const { data, isLoading, isError } = useGetTvMovieInfoQuery(id)

    if (isLoading) { return <div>Loading...</div> }
    if (isError) { return <div>Error...</div> }
    if (!data) { return null }

    const {
        backdrop_path,
        poster_path,
        name,
        original_name,
        genres,
        episode_run_time,
        overview,
        created_by,
        seasons,
        last_episode_to_air,
        next_episode_to_air
    } = data




    return (
        <>
            <header className="page-movie-header">
                <div className="header-bg" style={{ backgroundImage: `url(${IMG_URL}${backdrop_path})` }} />
                <div className="container">
                    <div className="header-wrapper">
                        <span className="btn-back"></span>
                        <div className="header-poster">
                            <img src={`${IMG_URL}${poster_path}`} alt={name} />
                        </div>
                        <div className="header-content">
                            <p className="movie-name">{name}</p>
                            <p className="movie-subname"><i>{original_name}</i></p>
                            <p className="movie-info">
                                <span className="genres">
                                    <span>{name}</span>
                                </span>
                                <span className="runtime">{episode_run_time}</span>
                            </p>
                            <p className="movie-overview">
                                {overview}
                            </p>
                            <p className="movie-created">
                                <span>{name} <br /> Creator</span>
                            </p>
                        </div>
                    </div>
                </div>
            </header>
            <div className="page-movie-content">
                <div className="container">
                    <p className="content-title">Seasons</p>
                    {/*{* { seasons {air_date,episode_count,name,poster_path} } *}*/}
                    <div className="cards-seasons">

                    </div>
                    <p className="content-title">Episodes</p>
                    <div className="cards-episodes">
                        {last_episode_to_air && (
                            <div className="episode-item episodes-last">
                                <div className="episode-wrapper">
                                    <div className="episodes-post">
                                        <img src={`${IMG_URL}${last_episode_to_air.still_path}`}
                                            alt={last_episode_to_air.name} />
                                    </div>
                                    <div className="episodes-content">
                                        <p className="episodes-name">Episodes name: {last_episode_to_air.name}</p>
                                        <p className="episodes-date"><i>Episodes
                                            date: {last_episode_to_air.air_date}</i></p>
                                        <p className="episodes-info">Season: {last_episode_to_air.season_number} Episode: {last_episode_to_air.episode_number} Runtime: {last_episode_to_air.runtime}</p>
                                        <p className="episodes-overview">{last_episode_to_air.overview}</p>
                                    </div>
                                </div>
                            </div>
                        )}
                        {next_episode_to_air && (
                            <div className="episode-item episodes-next">
                                <div className="episode-wrapper">
                                    <div className="episodes-post">
                                        <img src={`${IMG_URL}${next_episode_to_air.still_path}`}
                                            alt={next_episode_to_air.name} />
                                    </div>
                                    <div className="episodes-content">
                                        <p className="episodes-name">Episodes name: {next_episode_to_air.name}</p>
                                        <p className="episodes-date"><i>Episodes
                                            date: {next_episode_to_air.air_date}</i></p>
                                        <p className="episodes-info">Season: {next_episode_to_air.season_number} Episode: {next_episode_to_air.episode_number} Runtime: {next_episode_to_air.runtime}</p>
                                        <p className="episodes-overview">{next_episode_to_air.overview}</p>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </>

    )
}

export default TvMoviePage